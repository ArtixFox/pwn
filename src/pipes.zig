const std = @import("std");
const testing = std.testing;
const ArrayList = std.ArrayList;
const File = std.fs.File;
const mem = std.mem;
const net = std.net;

pub fn openLocal(fileArgs: []const []const u8, alloc: *std.mem.Allocator) !*std.ChildProcess {
    try std.io.getStdOut().writer().print("Trying to open: {s}", .{fileArgs[0]});
    var proc = try std.ChildProcess.init(fileArgs, alloc);
    proc.stdin_behavior = .Pipe;
    proc.stdout_behavior = .Pipe;
    return proc;
}

pub fn LocalSendline(proc: *std.ChildProcess, value: []const u8) !void {
    _ = try proc.stdin.?.write(value);
}
pub fn LocalSendArraylist(proc: *std.ChildProcess, arry: std.ArrayList(u8)) !void {
    try proc.stdin.?.writer().print("{s}", .{arry.items});
}

pub fn LocalReadline(proc: *std.ChildProcess, alloc: *std.mem.Allocator, delimiter: u8, max: u64) !?[]u8 {
    return try proc.stdout.?.reader().readUntilDelimiterOrEofAlloc(alloc, delimiter, max);
}

const context = struct {
    mutex: *std.Thread.Mutex,
    proc: *std.ChildProcess,
};

pub fn interactive(
    proc: *std.ChildProcess,
    alloc: *std.mem.Allocator,
    delimiter: u8,
    size: usize,
) anyerror!void {
    // Soo 2 threads??
    // T1 T2 T3
    //TODO make this work for fucks sake
    var mutex = std.Thread.Mutex{};
    var ctx = context{ .mutex = &mutex, .proc = proc };
    var t1 = try std.Thread.spawn(thread1, ctx);
    try mainLoop(&mutex, proc, alloc, delimiter, size);
    t1.wait();
}

fn thread1(ctx: context) !void {
    while (true) {
        var z: [std.math.maxInt(u17)]u8 = undefined;
        var len = try ctx.proc.stdout.?.reader().read(z[0..]);
        var hold = std.Thread.Mutex.acquire(ctx.mutex);
        defer hold.release();
        try std.io.getStdOut().writer().print("{s}", .{z[0..len]});
    }
}

fn mainLoop(mutex: *std.Thread.Mutex, proc: *std.ChildProcess, alloc: *std.mem.Allocator, delimiter: u8, max: usize) !void {
    while (true) {
        var c = (try std.io.getStdIn().reader().readUntilDelimiterOrEofAlloc(alloc, delimiter, max)) orelse "\n";
        var held = std.Thread.Mutex.acquire(mutex);
        defer held.release();
        try proc.stdin.?.writer().print("{s}\n", .{c[0..]});
    }
}
