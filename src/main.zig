const std = @import("std");
const testing = std.testing;
pub const pipes = @import("./pipes.zig");
pub const pack = @import("./packing.zig");

test "pack" {
    var s: [4]u8 = undefined;
    pack.padding(u32, s[0..], 0xdeadbeef, std.builtin.Endian.Little);
    // var bs = "A" ++ shit;
    try std.io.getStdOut().writer().print("{s}", .{std.fmt.fmtSliceHexLower(s[0..])});
    std.testing.expect(std.mem.eql(u8, s[0..], "\xef\xbe\xad\xde"));
}

test "pipes" {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var arena = std.heap.ArenaAllocator.init(&gpa.allocator);
    defer arena.deinit();

    var alloc = &arena.allocator;

    var a = try pipes.openLocal(&[_][]const u8{"/bin/bash"}, alloc);
    defer a.deinit();
    try a.spawn();
    var shit = "whoami\n";
    try pipes.LocalSendline(a, shit);
    var b = try pipes.LocalReadline(a, alloc, '\n', 10000);
    try std.io.getStdOut().writer().print("using localreadline {s}", .{b});
    try pipes.interactive(a, alloc, '\n', 100000000);
}
