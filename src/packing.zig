const std = @import("std");
pub fn padding(comptime typename: type, buffer: []u8, value: typename, endian: std.builtin.Endian) void {
    std.mem.writeIntSlice(typename, buffer, value, endian);
}

//pub fn packing(comptime destype: type, value: anytype, typed: anytype) destype {
//    var d = @as(typed, value);
//    return @bitCast(destype, d);
//}

test "try" {
    var s: [4]u8 = undefined;
    padding(u32, s[0..], 0xdeadbeef, std.builtin.Endian.Little);
    // var bs = "A" ++ shit;
    try std.io.getStdOut().writer().print("{s}", .{std.fmt.fmtSliceHexLower(s[0..])});
    std.testing.expect(std.mem.eql(u8, s[0..], "\xef\xbe\xad\xde"));
}
